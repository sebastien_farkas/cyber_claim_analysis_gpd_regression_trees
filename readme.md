# Cyber_claim_analysis_GPD_Regression_Trees

This repository contains the files which allow to reproduce the results of the paper Cyber claim analysis through Generalized Pareto Regression Trees.

The files can be gathered in four types:
######i) data files (which can be downloaded through the download repository),
######ii) one file that contained the functions for the building of trees,
######iii) files that allow to find the results highlighted in the paper.

### i) data files ###

######“Privacy_Rights_Clearinghouse-Data-Breaches-Export.csv” contained the PRC database dowloaded on January 23 2019 and used in the paper. 
######“truncdensity_0.1.0.tar.gz" a package needed for the simulations,
######"table_map_information_source.csv" that contain the map to gather sources,
######"NYSE_data.csv" that contain the data on compagnies quoted in the NYSE we use to build a virtual portfolio before assessing frequency,
######"table_map_NYSE_PRC_companies.csv" that contain the map of compagnies in the PRC database and quoted in the NYSE,
######"table_map_NYSE_PRC_sectors.csv" that contain the map between sectors in the PRC database and sectors available in NYSE data.

### ii) functions for the building of trees ###

######“function_trees.R” contained the functions named “user functions” used to modify the construction of the trees thanks to the rpart package.

### iii) files that allow to find the results highlighted in the paper ###

######“exec_PRC_loading.R” is used in following codes to load the database,

######“exec_PRC_frequency.R” allows to obtain the frequency results (number and typology of claims),

######“exec_PRC_severity.R” allows to compute descriptive statistics on severity,
######“exec_trees.R” allows to build the trees,
######“exec_stats_trees.R” allows to compute statistics on trees,

######“exec_vs_GAM_tail.R” allows to compare methodologies that both use Generalized Pareto Distribution but combined either with GAM either trees,
######“exec_vs_GAM_central.R” allows to complete the two latest methodologies by assessing the central part of the distribution,
######“exec_vs_GAM_output.R” allows to compute statistics on the different methodologies,

######“pricing_application.R” contains functions for portfolio simulation,
######“exec_pricing_application.R” aims to obtain the portfolios simulations,
######“actuarial_framework_application.R” contains functions for extracting descriptive statistics based on portfolio simulation,
######“exec_actuarial_framework_application.R” aims to obtain the portfolios statistics.