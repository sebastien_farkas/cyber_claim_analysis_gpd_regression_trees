# Reading of the PRC --------------------------------------------------------

PRC=read.csv2("./PRC.csv")

#Creation of the modality unknown for the Type.of.organization variable
PRC$Type.of.organization=as.character(PRC$Type.of.organization)
PRC$Type.of.organization[is.na(PRC$Type.of.organization)]="Unknown"
PRC$Type.of.organization=as.factor(PRC$Type.of.organization)

#Clearing of the strings Company
PRC$Company=str_to_lower(PRC$Company)
PRC$Company=str_replace_all(PRC$Company, "[[:punct:]]", " ")
PRC$Company=str_squish(PRC$Company)

#Aggregation of the PRC data base by Company
count_freq_PRC=PRC %>% 
  group_by(Company,Type.of.organization) %>% 
  summarise(nb = n()) %>%
  arrange(desc(nb))

#Number of companies having k reports in PRC database

stats_desc_PRC=data.frame(n_breaches=names(table(count_freq_PRC$nb)),n_companies=as.numeric(table(count_freq_PRC$nb)),p_copmpanies=as.numeric(table(count_freq_PRC$nb))/sum(table(count_freq_PRC$nb)))

write.csv2(stats_desc_PRC,"table_12_PRC_part.csv")

# NYSE portfolio construction -------------------------------------------------

#First, the companies that have been breached 

#NYSE
match_nyse_prc=read.csv2("./table_map_NYSE_PRC_companies.csv")
match_nyse_prc=match_nyse_prc[match_nyse_prc$Human.confirmation==1,]
match_nyse_prc$prc=str_to_lower(match_nyse_prc$prc)
match_nyse_prc$prc=str_replace_all(match_nyse_prc$prc, "[[:punct:]]", " ")
match_nyse_prc$prc=str_squish(match_nyse_prc$prc)

NYSE=PRC[PRC$Company %in% match_nyse_prc$prc,]

NYSE$Type.of.organization[as.character(NYSE$Type.of.organization)=="EDU"]="BSO"
NYSE$Type.of.organization[as.character(NYSE$Type.of.organization)=="NGO"]="BSO"

#Aggregation of the PRC data base by Company
count_freq_NYSE=NYSE %>% 
  group_by(Company,Type.of.organization) %>% 
  summarise(nb = n()) %>%
  arrange(desc(nb))

count_freq_NYSE=as.data.frame(count_freq_NYSE)

#Second, the companies that have not been breached 

NYSE_portfolio=read.csv("./NYSE_data.csv")
to_match=read.csv2("./table_map_NYSE_PRC_sectors.csv")
NYSE_portfolio$Type=as.factor(to_match$PRC.Type.of.organization[match(paste0(NYSE_portfolio$Sector,NYSE_portfolio$industry),paste0(to_match$Sector,to_match$industry))])

NYSE_portfolio$Name=str_to_lower(NYSE_portfolio$Name)
NYSE_portfolio$Name=str_replace_all(NYSE_portfolio$Name, "[[:punct:]]", " ")
NYSE_portfolio$Name=str_squish(NYSE_portfolio$Name)

NYSE_portfolio=unique(NYSE_portfolio[,c("Name","Type")])

match_nyse_prc=read.csv2("./table_map_NYSE_PRC_companies.csv")
match_nyse_prc=match_nyse_prc[match_nyse_prc$Human.confirmation==1,]

match_nyse_prc$prc=str_to_lower(match_nyse_prc$prc)
match_nyse_prc$prc=str_replace_all(match_nyse_prc$prc, "[[:punct:]]", " ")
match_nyse_prc$prc=str_squish(match_nyse_prc$prc)

match_nyse_prc$nyse=str_to_lower(match_nyse_prc$nyse)
match_nyse_prc$nyse=str_replace_all(match_nyse_prc$nyse, "[[:punct:]]", " ")
match_nyse_prc$nyse=str_squish(match_nyse_prc$nyse)

count_freq_NYSE_0=cbind(NYSE_portfolio[is.na(match(NYSE_portfolio$Name,count_freq_NYSE$Company)),],nb=0)

names(count_freq_NYSE_0)=names(count_freq_NYSE)

count_freq_NYSE=rbind(count_freq_NYSE,count_freq_NYSE_0)

count_freq_NYSE$Type.of.organization=as.factor(as.character(count_freq_NYSE$Type.of.organization))

stats_desc_PRC=data.frame(n_breaches=names(table(count_freq_NYSE$nb)),n_companies=as.numeric(table(count_freq_NYSE$nb)),p_copmpanies=as.numeric(table(count_freq_NYSE$nb))/sum(table(count_freq_NYSE$nb)))

write.csv2(stats_desc_PRC,"table_12_NYSE_part.csv")

# NYSE >=1 ----------------------------------------------------------------

#Selection of companies having at least 2 reports
count_freq_NYSE=count_freq_NYSE[count_freq_NYSE$nb>=1,]
  
#Deletion of the types G0V and unknown that does not have enough data 
count_freq_NYSE=count_freq_NYSE[!(as.character(count_freq_NYSE$Type.of.organization) %in% c("GOV","Unknown")),]

#Inverse logit function
inverse_logit=function(x){return(1/(1+exp(-x)))}

#Creation of a 0 truncated Poisson family
gen.trun(par = c(0), family = "PO", name = "tr0",
         type = c("left"), 
         varying = FALSE)
         
#Creation of a 0 truncated Geometric family
gen.trun(par = c(0), family = "GEOMo", name = "tr0", 
         type = c("left"), 
         varying = FALSE)

res_freq_NYSE_pois=gamlss(formula= nb ~ Type.of.organization - 1,
                  data=count_freq_NYSE,
                  family=POtr0(mu.link = log),
                  control=gamlss.control(trace = FALSE))
   
res_freq_NYSE_geom=gamlss(formula= nb ~ Type.of.organization - 1,
                  data=count_freq_NYSE,
                  family=GEOMotr0(mu.link = logit),
                  control=gamlss.control(trace = FALSE))


write.csv2(data.frame(pois=c(-res_freq_NYSE_pois$G.deviance,
res_freq_NYSE_pois$aic),geom=c(-res_freq_NYSE_geom$G.deviance,res_freq_NYSE_geom$aic)),"table_13_NYSE.csv")

write.csv2(data.frame(lower_bound=1/(1+1/8*(1/inverse_logit(confint(object = res_freq_NYSE_geom,level = 0.95))[,1]-1)),
				estimate=1/(1+1/8*(1/inverse_logit(res_freq_NYSE_geom$mu.coefficients)-1)),
				upper_bound=1/(1+1/8*(1/inverse_logit(confint(object = res_freq_NYSE_geom,level = 0.95))[,2]-1)),
				expectation=1/(1/(1+1/8*(1/inverse_logit(res_freq_NYSE_geom$mu.coefficients)-1)))-1),"table_14_NYSE.csv")

write.csv2(res_freq_NYSE_geom$mu.coefficients,"fit_geom_NYSE.csv")
write.csv2(vcov(res_freq_NYSE_geom),"fit_geom_NYSE_vcov.csv")

# PRC >=2 -----------------------------------------------------------------


#Selection of companies having at least 2 reports
count_freq_PRC=count_freq_PRC[count_freq_PRC$nb>=2,]

#Creation of a 0 and 1 truncated Poisson family
gen.trun(par = c(1), family = "PO", name = "tr1",
         type = c("left"), 
         varying = FALSE)
  
#Creation of a 0 and 1 truncated Geometric family
gen.trun(par = c(1), family = "GEOMo", name = "tr1", 
         type = c("left"), 
         varying = FALSE)
         
         
res_freq_PRC_pois=gamlss(formula= nb ~ Type.of.organization - 1,
                  data=count_freq_PRC,
                  family=POtr1(mu.link = log),
                  control=gamlss.control(trace = FALSE))
   
res_freq_PRC_geom=gamlss(formula= nb ~ Type.of.organization - 1,
                  data=count_freq_PRC,
                  family=GEOMotr1(mu.link = logit),
                  control=gamlss.control(trace = FALSE))


write.csv2(data.frame(pois=c(-res_freq_PRC_pois$G.deviance,
res_freq_PRC_pois$aic),geom=c(-res_freq_PRC_geom$G.deviance,res_freq_PRC_geom$aic)),"table_13_PRC.csv")


write.csv2(data.frame(lower_bound=1/(1+1/8*(1/inverse_logit(confint(object = res_freq_PRC_geom,level = 0.95))[,1]-1)),
				estimate=1/(1+1/8*(1/inverse_logit(res_freq_PRC_geom$mu.coefficients)-1)),
				upper_bound=1/(1+1/8*(1/inverse_logit(confint(object = res_freq_PRC_geom,level = 0.95))[,2]-1)),
				expectation=1/(1/(1+1/8*(1/inverse_logit(res_freq_PRC_geom$mu.coefficients)-1)))-1),"table_14_PRC.csv")


write.csv2(res_freq_PRC_geom$mu.coefficients,"fit_geom_PRC.csv")
write.csv2(vcov(res_freq_PRC_geom),"fit_geom_PRC_vcov.csv")

# BEGINING LASSO MULTINOMIAL ----------------------------------------------

#Response variable: the type of breach
y=PRC$Type.of.breach[!is.na(PRC$Type.of.breach)]
#Explanatories variable: the business sector (without the NA that is taken as the Intercept)
x=as.matrix(dummy_cols(.data = PRC$Type.of.organization)[!is.na(PRC$Type.of.breach),c(".data_BSO",
                                                                                      ".data_MED",
                                                                                      ".data_BSF",
                                                                                      ".data_NGO",
                                                                                      ".data_BSR",
                                                                                      ".data_EDU",
                                                                                      ".data_GOV")])

#par(mfrow=c(2,1))
set.seed(2)
fit=glmnet(x, y, family = "multinomial", type.multinomial = "ungrouped")
#plot(fit, xvar = "lambda", label = TRUE, type.coef = "2norm")
cvfit=cv.glmnet(x, y, family="multinomial", type.multinomial = "ungrouped", parallel = FALSE,nfolds=10,type.measure="class")
#cvfit=cv.glmnet(x, y, family="multinomial", type.multinomial = "ungrouped", parallel = FALSE,nfolds=10,type.measure="deviance")
#cvfit=cv.glmnet(x, y, family="multinomial", type.multinomial = "ungrouped", parallel = FALSE,nfolds=10,type.measure="mse")
#plot(cvfit)

#cvfit$lambda.min
#coef(cvfit, s = "lambda.min")
#cvfit$lambda.1se
#coef(cvfit, s = "lambda.1se")

name_col=c("CARD","DISC","HACK","INSD","PHYS","PORT","STAT")
name_row=c("(Intercept)","BSO","MED","BSF","NGO","BSR","EDU","GOV")
coef_cvfit=matrix(nrow = 8,ncol = 7,dimnames = list(name_row,name_col))
coef_multinomial=matrix(nrow = 8,ncol = 7,dimnames = list(name_row,name_col))
for(i in 1:length(name_col)){
  coef_cvfit[,i]=coef(cvfit, s = "lambda.1se")[[name_col[i]]][,1]
}
for(i in 1:length(name_row)){
  for(j in 1:length(name_col)){
    coef_multinomial[i,j]=exp(coef_cvfit[1,j]+coef_cvfit[i,j])/sum(exp(coef_cvfit[1,]+coef_cvfit[i,]))
  }
}

write.csv2(coef_cvfit,"table_15.csv")
write.csv2(coef_multinomial,"SIMU_coef_multinomial_lasso.csv")
