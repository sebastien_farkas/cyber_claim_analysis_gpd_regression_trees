# Comparison of GPD, CART GPD and GAM GPD ---------------------------------

PRC=read.csv2("./PRC.csv")
PRC=PRC[PRC$Total.Records>0,]

#Threshold for extremes values 
u=27799

#Standardisation des donn??es
#Exclusion des NA (qui sont des 0)
PRC_extremes=PRC[PRC$Total.Records>u,]
#Computation of the excesses 
PRC_extremes$Total.Records=PRC_extremes$Total.Records-u
#Division of the excesses by 100000 for computationnal purposes
PRC_extremes$Total.Records=PRC_extremes$Total.Records/100000

PRC_extremes_naomit=na.omit(PRC_extremes)

# GPD ---------------------------------------------------------------------

GPD=evir::gpd(PRC_extremes_naomit$Total.Records, threshold = 0, method = "ml")
save(GPD,file="GPD.RData")

# CART GPD ----------------------------------------------------------------

set.seed(4)

GPRT_unpruned_naomit=rpart(data = PRC_extremes_naomit,
                           formula = Total.Records ~  
                             Gather.Information.Source +
                             Type.of.breach + 
                             Type.of.organization +
                             year(Date.Made.Public),
                           method = GPD_method,
                           control = rpart.control(cp=0,minsplit=60, minbucket=20, maxcompete=10, maxsurrogate=10),
                           xval=0)

alpha=cross_val_gprt(data = PRC_extremes_naomit,arbre = GPRT_unpruned_naomit,n_fold = 10)
GPRT_naomit=prune(GPRT_unpruned_naomit,cp=alpha/GPRT_unpruned_naomit$frame[1,"dev"])

plot(GPRT_naomit,margin=0.025)
try(text(GPRT_naomit,minlength=3,cex=0.75),silent=TRUE)
save(GPRT_naomit,file="pruned_GPRT_naomit.RData")

# GAM GPD -----------------------------------------------------------------

PRC_extremes_naomit$Date.Made.Public=year(PRC_extremes_naomit$Date.Made.Public)-2005


eps=0.005

fximu=c(as.formula("~ 1"),
        as.formula("~ Type.of.organization -1"),
        as.formula("~ Type.of.breach -1"),
        as.formula("~ Gather.Information.Source -1"),
        as.formula("~  Type.of.organization + Type.of.breach -1"),
        as.formula("~ Type.of.organization + Gather.Information.Source -1"),
        as.formula("~  Type.of.breach + Gather.Information.Source -1"),
        as.formula("~ Type.of.organization + Type.of.breach + Gather.Information.Source -1"),
        as.formula("~ Date.Made.Public + 1"),
        as.formula("~ Date.Made.Public + Type.of.organization - 1"),
        as.formula("~ Date.Made.Public + Type.of.breach - 1"),
        as.formula("~ Date.Made.Public + Gather.Information.Source - 1"),
        as.formula("~ Date.Made.Public + Type.of.organization + Gather.Information.Source -1"),
        as.formula("~  Date.Made.Public + Type.of.breach + Gather.Information.Source -1"),
        as.formula("~  Date.Made.Public + Type.of.breach + Type.of.organization -1"),
        as.formula("~ Date.Made.Public + Type.of.organization + Type.of.breach + Gather.Information.Source -1"),
        as.formula("~ s(Date.Made.Public) + 1"),
        as.formula("~ s(Date.Made.Public) + Type.of.organization - 1"),
        as.formula("~ s(Date.Made.Public) + Type.of.breach - 1"),
        as.formula("~ s(Date.Made.Public) + Gather.Information.Source - 1"),
        as.formula("~ s(Date.Made.Public) + Type.of.organization + Gather.Information.Source -1"),
        as.formula("~  s(Date.Made.Public) + Type.of.breach + Gather.Information.Source -1"),
        as.formula("~  s(Date.Made.Public) + Type.of.breach + Type.of.organization -1"),
        as.formula("~ s(Date.Made.Public) + Type.of.organization + Type.of.breach + Gather.Information.Source -1")
)  


LLAIC3=matrix(nrow = 576,ncol=4)  
c=1
for(i in 1:24){
  for(j in 1:24){
    
    fit <- try(gamGPDfit(PRC_extremes_naomit, threshold=0, datvar="Total.Records", xiFrhs=fximu[i][[1]],
                         nuFrhs=fximu[j][[1]], eps.xi=eps, eps.nu=eps,niter = 100,progress = FALSE),silent = TRUE)
    
    if(class(fit)=="list"){
      LLAIC3[c,]=c(i,j,fit$logL,-2*fit$logL+2*(length(fit$xiObj$coefficients)+length(fit$nuObj$coefficients)))
    }
    c=c+1
  }
}

fitGAMGPD <- gamGPDfit(PRC_extremes_naomit, threshold=0, datvar="Total.Records", xiFrhs=fximu[LLAIC3[LLAIC3[,4]==min(LLAIC3[,4]),1]][[1]],
                       nuFrhs=fximu[LLAIC3[LLAIC3[,4]==min(LLAIC3[,4]),2]][[1]], eps.xi=eps, eps.nu=eps,niter = 100,progress = FALSE)

save(PRC_extremes_naomit,file="data_for_fit_tail_part_fitGAMGPD.RData")
save(fitGAMGPD,file="fit_tail_part_fitGAMGPD.RData")
